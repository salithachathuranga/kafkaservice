package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import play.libs.Json;
import play.mvc.*;
import services.ConsumerService;
import services.MessageService;
import services.ProducerService;

import java.io.IOException;
import java.text.ParseException;

public class KafkaController extends Controller {

    @Inject
    public ProducerService producerService;

    @Inject
    public ConsumerService consumerService;

    @Inject
    public MessageService messageService;

    public Result postMessage(Http.Request request) throws IOException, ParseException {
        JsonNode res = messageService.sendMessageToEs(producerService.postMessageToKafka(request.body().asJson()));
        return ok(Json.toJson(res));
    }

//    public Result subscribeMessage() {
//        consumerService.subscribeMessageFromKafka();
//        return ok("done");
//    }

}
