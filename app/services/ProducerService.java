package services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import play.libs.Json;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Properties;

public class ProducerService {

    public static String TOPIC = "test-kafka-topic";

    public Properties setConfigs() {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return properties;
    }

    public Producer<String, String> createProducer() {
        return new KafkaProducer<>(setConfigs());
    }

    public JsonNode postMessageToKafka(JsonNode body) throws ParseException {
        ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC, body.get("id").asText(), body.get("status").asText());
        Producer<String, String> producer = createProducer();
        producer.send(record);
        ObjectNode msgObject = Json.newObject();
        msgObject.put("id", record.key());
        msgObject.put("status", record.value());
        LocalDate localDate = LocalDate.now();
        LocalTime localTime = LocalTime.now();
        System.out.println(DateTimeFormatter.ofPattern("yyy/MM/dd").format(localDate));
        msgObject.put("date", localDate.toString());
        msgObject.put("time", localTime.toString());
        System.out.println(msgObject);
        producer.close();
        return msgObject;
    }
}
