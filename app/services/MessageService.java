package services;

import bo.Message;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.ImplementedBy;
import org.apache.http.HttpHost;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.rest.RestStatus;
import play.libs.Json;
import play.mvc.BodyParser;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;

public class MessageService {

    private final String INDEX_NAME = "es-kafka-test";
    private final String DOC_TYPE = "message";

    private final RestClientBuilder clientBuilder = RestClient.builder(new HttpHost("localhost", 9200));
    private final RestHighLevelClient highLevelClient = new RestHighLevelClient(
            clientBuilder.setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder)
    );

    @BodyParser.Of(BodyParser.Json.class)
    public JsonNode sendMessageToEs(JsonNode body) throws IOException {
        ObjectNode result = Json.newObject();
        Message msg = Json.fromJson(body, Message.class);
        LocalDate localDate = LocalDate.now();
        LocalTime localTime = LocalTime.now();
        msg.setDate(localDate.toString());
        msg.setTime(localTime.toString());
        IndexRequest request = new IndexRequest(INDEX_NAME).type(DOC_TYPE).id(Integer.toString(msg.getId()))
                .source(Json.stringify(Json.toJson(msg)), XContentType.JSON);
        IndexResponse response = highLevelClient.index(request, RequestOptions.DEFAULT);
        System.out.println(response.status());
        if (response.getIndex() != null && response.status() == RestStatus.CREATED) {
            result.put("message", "Message Created ID: " + msg.getId());
        } else {
            result.put("message", "Message Not Created");
        }
        return result;
    }
}
